**Expense Reimbursement System (JwA)**

**Project Description**

A full stack reimburesement system allowing for employees to view and submit requests and for managers to approve or deny them.

**Technologies Used**

Java 8
mariaDb
Hibernate
Javalin
JavaScript
HTML
CSS

**Features**

Login validation based on role
Managers are able to filter by employee name
Managers able to approve or deny requests
Employees able to create requests
Dynamically updating statistics for managers

**To-do list:**

Implement more robust tests.
Fix routing issue when log in fails.

